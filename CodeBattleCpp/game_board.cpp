#include <map>
#include <array>
#include <iostream>
#include "interpolation.hpp"
#include "game_board.hpp"

int game_board::can_shoot = 0;
static const std::array<vec, 4> vec_directions = {
		vec(0,1),
		vec(0,-1),
		vec(-1,0),
		vec(1,0)
};

void game_board::ensure_dangerzones(vec from) {//get_bullet_velocities
	std::vector<vec> safe_dirs;
	int bullets_around = 0;
	for (auto i = 0; i < 4; i++) {
		if (get_element_at(from + vec_directions[i]).value() == tile::NONE)
			safe_dirs.push_back(vec_directions[i]);
		if (get_element_at(from + vec_directions[i]).value().is_bullet() || 
			get_element_at(from + vec_directions[i]).value().is_player() ||
			get_element_at(from + vec_directions[i]).value().is_enemy_bot())
			bullets_around++;
	}
	if (bullets_around < 2)
		return;
	if (!safe_dirs.size())
		return;
	std::vector<player_command> possible_cmds;
	for (auto& elem : safe_dirs)
	{
		if (elem.x > 0)
			possible_cmds.push_back(player_command::right);
		if (elem.x < 0)
			possible_cmds.push_back(player_command::left);
		if (elem.y > 0)
			possible_cmds.push_back(player_command::down);
		if (elem.y < 0)
			possible_cmds.push_back(player_command::up);
	}
	if (std::find(possible_cmds.begin(), possible_cmds.end(), next_action_) != possible_cmds.end())
		return;
	std::cout << "I think i'm in danger \n";
	next_action_ = possible_cmds.front();
}

void game_board::find_next_direction(vec closest_enemy, vec at) {
	if (opt_path.size() > 0) {
		auto to = opt_path[0];
		if (to.x > at.x) next_action_ = player_command::right;
		if (to.x < at.x) next_action_ = player_command::left;
		if (to.y > at.y) next_action_ = player_command::down;
		if (to.y < at.y) next_action_ = player_command::up;
	}
}

void game_board::bfs(vec at) {
	can_shoot = std::clamp(can_shoot-1, 0, can_shoot);
	if (at.x <= 0) {
		can_shoot = 0;
		return;
	}
	std::cout << "Ticks to shoot: " << can_shoot << std::endl;
	std::vector<vec> closest_elems;
	std::vector<int> d(graph.size(), std::numeric_limits<int>::max()), p(graph.size());
	d[at.y * map.size() + at.x] = 0;
	opt_path.clear();
	if (!has_enemy_at_axis(at) || can_shoot > 0) {
		std::vector<char> u(graph.size());
		vec closest_en(-1, -1);
		for (int i = 0; i < graph.size(); ++i) {
			int v = -1;
			for (int j = 0; j < graph.size(); ++j)
				if (!u[j] && (v == -1 || d[j] < d[v]))
					v = j;
			if (d[v] == std::numeric_limits<int>::max())
				break;
			u[v] = true;

			for (size_t j = 0; j < graph[v].size(); ++j) {
				auto to = graph[v][j].first;
				//auto element = get_element_at(to);
				if (has_enemy_at_axis(to)) {
					if(can_shoot == 0)
						closest_elems.push_back(to);
				}
				else if(can_shoot != 0 && get_element_at(to).value() == tile::NONE)
					closest_elems.push_back(to);

				int len = graph[v][j].second;
				if (d[v] + len < d[to.y * map.size() + to.x]) {
					d[to.y * map.size() + to.x] = d[v] + len;
					p[to.y * map.size() + to.x] = v;
				}
			}
		}
		int max_gold = 0;
		for (auto& elem : closest_elems) {
			for (auto v = elem; v != at; v = vec(p[v.y * map.size() + v.x] % map.size(), p[v.y * map.size() + v.x] / map.size()))
				opt_path.push_back(v);
			break;
		}
		std::reverse(opt_path.begin(), opt_path.end());

		find_next_direction(closest_en, at);
	}
	if (!opt_path.size() || !get_element_at(opt_path.front()).value().is_brick())
		cmd = shoot_command::none;
	else cmd = shoot_command::after;
	if(can_shoot == 0)
		find_near_targets(at);
	ensure_dangerzones(at);
	if (cmd != shoot_command::none) {
		can_shoot = 4;
		/*if (next_action_ == player_command::kill_self && cmd == shoot_command::before)
			next_action_ = static_cast<player_command>(static_cast<int>(get_element_at(at).value().get_tank_direction()) ^ 1);*/
	}
}

std::vector<std::pair<vec, int>> game_board::scan_shootline(vec from, tile direction) {
	
	if (get_element_at(from).value_or(tile::BULLET).is_bullet() ||
		get_element_at(from).value_or(tile::CONSTRUCTION).is_brick() || 
		get_element_at(from).value_or(tile::BATTLE_WALL).is_unbreakable())
		return std::vector<std::pair<vec, int>>();
	std::vector<std::pair<vec, int>> targets;
	for (auto& elem : vec_directions) {
		auto cost = 1;
		auto target = scan_line(from, elem);
		if (target.has_value()) 
			targets.push_back({ target.value(), cost });
	}
	/**/
	return targets;
}

void game_board::find_near_targets(vec from) {
	std::vector<std::pair<vec, int>> results;
	for (auto i = from.y; i <= from.y; i++) {
		auto res1 = scan_shootline(vec(from.x,i), tile::TANK_RIGHT);
		auto res = scan_shootline(vec(from.x, i), tile::TANK_LEFT);
		if (i != from.y) {
			for (auto& elem : res)
				elem.second += 1;
			for (auto& elem : res1)
				elem.second += 1;
		}
		results.insert(results.end(), res1.begin(), res1.end());
		results.insert(results.end(), res.begin(), res.end());
	}
	for (auto j = from.x; j <= from.x; j++) {
		auto res1 = scan_shootline(vec(j, from.y), tile::TANK_UP);
		auto res = scan_shootline(vec(j, from.y), tile::TANK_DOWN);
		if (j != from.x) {
			for (auto& elem : res)
				elem.second += 1; 
			for (auto& elem : res1)
				elem.second += 1;
		}
		results.insert(results.end(), res1.begin(), res1.end());
		results.insert(results.end(), res.begin(), res.end());
	}
	std::sort(results.begin(), results.end(), [from](const std::pair<vec, int>& lhs, const std::pair<vec, int>& rhs) {
		if(rhs.second != lhs.second)
			return rhs.second > lhs.second;
		return rhs.first.dist_to(from) > lhs.first.dist_to(from);
		});
	std::unique(results.begin(), results.end(), [](const std::pair<vec, int>& lhs, const std::pair<vec, int>& rhs) {
		return rhs.second == lhs.second;
		});
	
	if (!results.size())
		return;
	auto& front = results.front();
	if (front.second == 1) {
		
		auto command = tile::NONE;
		if (front.first.y == from.y && front.first.x > from.x) command = tile::TANK_RIGHT;
		if (front.first.y == from.y && front.first.x < from.x) command = tile::TANK_LEFT;
		if (front.first.x == from.x && front.first.y > from.y) command = tile::TANK_DOWN;
		if (front.first.x == from.x && front.first.y < from.y) command = tile::TANK_UP;
		if (get_element_at(from) == command) {
			cmd = shoot_command::before;
			return;
		}
		else
			cmd = shoot_command::after;
		if (front.first.y == from.y && front.first.x > from.x) next_action_ = player_command::right;
		if (front.first.y == from.y && front.first.x < from.x) next_action_ = player_command::left;
		if (front.first.x == from.x && front.first.y > from.y) next_action_ = player_command::down;
		if (front.first.x == from.x && front.first.y < from.y) next_action_ = player_command::up;
	}
}

bool game_board::has_enemy_at_axis(vec from) {
	std::vector<std::pair<vec, int>> results;
	for (auto i = from.y; i <= from.y; i++) {
		auto res1 = scan_shootline(vec(from.x, i), tile::TANK_RIGHT);
		auto res = scan_shootline(vec(from.x, i), tile::TANK_LEFT);
		if (i != from.y) {
			for (auto& elem : res)
				elem.second += 1;
			for (auto& elem : res1)
				elem.second += 1;
		}
		results.insert(results.end(), res1.begin(), res1.end());
		results.insert(results.end(), res.begin(), res.end());
	}
	for (auto j = from.x; j <= from.x; j++) {
		auto res1 = scan_shootline(vec(j, from.y), tile::TANK_UP);
		auto res = scan_shootline(vec(j, from.y), tile::TANK_DOWN);
		if (j != from.x) {
			for (auto& elem : res)
				elem.second += 1;
			for (auto& elem : res1)
				elem.second += 1;
		}
		results.insert(results.end(), res1.begin(), res1.end());
		results.insert(results.end(), res.begin(), res.end());
	}
	std::sort(results.begin(), results.end(), [from](const std::pair<vec, int>& lhs, const std::pair<vec, int>& rhs) {
		if (rhs.second != lhs.second)
			return rhs.second > lhs.second;
		return rhs.first.dist_to(from) > lhs.first.dist_to(from);
		});
	std::unique(results.begin(), results.end(), [](const std::pair<vec, int>& lhs, const std::pair<vec, int>& rhs) {
		return rhs.second == lhs.second;
		});

	if (!results.size())
		return false;
	auto& front = results.front();
	return front.second == 1;
}

std::optional<vec> game_board::scan_line(vec from, vec velocity) {
	auto myself = abs(from.x) + abs(from.y);
	while (from > 0 && from < map.size()) {
		if (get_element_at(from).value().is_brick() || get_element_at(from).value().is_unbreakable() || get_element_at(from).value().is_bullet())
			return std::nullopt;
		if (get_element_at(from).value().is_player()/* || get_element_at(from).value().is_enemy_bot()*/) {
			if (myself < 3)
				return std::nullopt;
			else
				return from;
		}
		myself += abs(velocity.x) + abs(velocity.y);
		from += velocity;
	}
	return std::nullopt;
}

void game_board::dfs(vec at) {
	auto pos = get_element_at(at);
	if (pos.has_value() && (pos.value() == tile::BATTLE_WALL || pos.value().is_bullet()))
		return;
	//if (at.x == 42 && at.y == 34)
	//	pos = get_element_at(at);
	auto i = at.y;
	for (auto j = at.x - 1; j <= at.x + 1; j++) {
		if (j == at.x) continue;
		auto val = get_element_at({ j,i });
		if (val.has_value()) {
			auto element = val.value();
			auto offset = 0;
			switch (element.get_type()) {
			case tile::type_unbreakable:
			case tile::type_bullet:
				break;
			case tile::type_breakable:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i), element.get_cost(j < at.x ? player_command::right : player_command::left) });
				break;
			case tile::type_enemy:
			case tile::type_player:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i),1 });
				break;
			default:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i),1 });
			}
		}
	}
	auto j = at.x;
	for (i = at.y - 1; i <= at.y + 1; i++) {
		if (i == at.y) continue;

		auto val = get_element_at({ j,i });
		if (val.has_value()) {
			auto element = val.value();
			auto offset = 0;
			switch (element.get_type()) {
			case tile::type_unbreakable:
			case tile::type_bullet:
				break;
			case tile::type_player:
			case tile::type_enemy:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i),1 });
				break;
			case tile::type_breakable:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i), element.get_cost(i < at.y ? player_command::up : player_command::down) });
				break;
			default:
				graph[at.y * map.size() + at.x].push_back({ vec(j, i),  /*i > at.y ? 3 :*/ 1 });
			}
		}
	}
	//}
}

game_board::game_board(std::vector<std::vector<tile>>& map)
	: map(map) {
	this->graph.resize(map.size() * map.size());
	for (auto i = 0; i < map.size(); i++) {
		for (auto j = 0; j < map[i].size(); j++) {
			auto val = get_element_at(vec(j, i));
			if (val.has_value()) {
				dfs(vec(j, i));
			}
		}
	}
	next_action_ = player_command::kill_self;
	//if (!is_game_over())
	//	bfs(get_myself());

}

vec game_board::get_myself() {
	std::list<vec> result = find_all(tile::TANK_DOWN);
	result.splice(result.end(), find_all(tile::TANK_LEFT));
	result.splice(result.end(), find_all(tile::TANK_RIGHT));
	result.splice(result.end(), find_all(tile::TANK_UP));
	return result.size() ? result.front() : vec(0,0);
}

bool game_board::is_game_over() {
	return false;
}

bool game_board::has_element_at(vec point, tile element) {
	auto val = get_element_at(point);
	return val.has_value() && val.value() == element;
}

std::optional<tile> game_board::get_element_at(vec point) {
	if (point.isOutOfBoard(map.size()))
		return std::nullopt;
	return map[point.y][point.x];
}

std::list<vec> game_board::find_all(tile element) {
	std::list<vec> result;
	for (uint32_t j = 0; j < map.size(); j++)
	{
		for (uint32_t i = 0; i < map.size(); i++)
		{
			if (map[j][i] == element) {
				result.push_back(vec(i, j));
			}
		}
	}
	return result;
}

bool game_board::is_near_to(vec point, tile element) {
	if (point.isOutOfBoard(map.size())) {
		return false;
	}
	return has_element_at(point.shiftBottom(), element)
		|| has_element_at(point.shiftTop(), element)
		|| has_element_at(point.shiftLeft(), element)
		|| has_element_at(point.shiftRight(), element);
}

game_board::~game_board()
{

}

