#pragma once
#include <memory>
#include <list>
#include <map>
#include <array>
#include "game_board.hpp"

class interpolation {
public:
	static __forceinline interpolation* instance() {
		static auto inst = new interpolation();
		return inst;
	}
	interpolation();
	~interpolation();
	void add_field(std::vector<std::vector<tile>>& map) {
		old_fields.push_back(std::make_shared<game_board>(map));
		if (old_fields.size() > 5)
			old_fields.pop_front();
	}
	void predict_bullets(vec pos, vec velocity);
	void predict_field();
	player_command next_action() {
		if (predicted_.get())
			return predicted_->current_act();
		return player_command::kill_self;
	}
	shoot_command next_shoot_cmd() {
		if (predicted_.get())
			return predicted_->current_shoot_act();
		return shoot_command::none;
	}
	std::shared_ptr<game_board>& get_pred_board() { return predicted_; }
	std::map<std::string, std::pair<int, int>>& get_old_positions() { return old_positions; }
	std::map<std::pair<int, int>, vec>& get_tank_velsref() { return tank_vels; }
	std::map<std::pair<int, int>, std::list<vec>>& get_bullet_velocities() { return bullet_velocities; }
protected:
	std::map<std::pair<int, int>, std::list<vec>> bullet_velocities;
	std::map<std::pair<int, int>, vec> tank_vels;
	std::map<std::string, std::pair<int, int>> old_positions;
	std::list<player_command> old_cmd;
	std::vector<std::vector<tile>> pred_field;
	std::shared_ptr<game_board> predicted_ = nullptr;
	std::list<std::shared_ptr<game_board>> old_fields;
};