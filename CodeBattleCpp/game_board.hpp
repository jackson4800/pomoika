﻿#pragma once

#include "tile.hpp"
#include "vec.hpp"
#include "player_command.hpp"
#include <list>
#include <optional>
#include <queue>
#include <stack>
#include <vector>

class game_board
{
	static int can_shoot;
public:
	game_board(std::vector<std::vector<tile>>& map);
	vec get_myself();
	bool is_game_over();
	bool has_element_at(vec point, tile element);
	std::optional<tile> get_element_at(vec point);
	//void printBoard();
	std::list<vec> find_all(tile element);
	std::optional<vec> scan_line(vec from, vec velocity);
	std::vector<std::pair<vec, int>> scan_shootline(vec from, tile direction);
	void find_near_targets(vec from);
	bool is_near_to(vec point, tile element);
	void dfs(vec at);
	void bfs(vec at);
	~game_board();
	player_command& current_act() {
		return next_action_;
	}
	shoot_command& current_shoot_act() {
		return cmd;
	}
	const std::vector<vec>& get_current_path() {
		return opt_path;
	}
	std::vector<std::vector<tile>>& get_board() {
		return map;
	}
	static void reset_shooting_timings() { can_shoot = 0; }
	bool has_enemy_at_axis(vec from);
	void ensure_dangerzones(vec from);
private:
	void find_next_direction(vec closest_enemy, vec at);
	std::vector<std::vector<tile>> map;
	std::vector<std::vector<std::pair<vec, int>>> graph;
	std::vector<vec> opt_path;
	player_command next_action_;
	shoot_command cmd;
};