﻿#pragma once
#include "player_command.hpp"
#include <cstdint>

class tile {
public:
	enum tile_id_ : uint16_t
	{
		Unk = 0,
		CONSTRUCTION = L'╬',

		CONSTRUCTION_DESTROYED_DOWN = L'╩',
		CONSTRUCTION_DESTROYED_UP = L'╦',
		CONSTRUCTION_DESTROYED_LEFT = L'╠',
		CONSTRUCTION_DESTROYED_RIGHT = L'╣',
		CONSTRUCTION_DESTROYED_DOWN_TWICE = L'╨',
		CONSTRUCTION_DESTROYED_UP_TWICE = L'╥',
		CONSTRUCTION_DESTROYED_LEFT_TWICE = L'╞',
		CONSTRUCTION_DESTROYED_RIGHT_TWICE = L'╡',
		CONSTRUCTION_DESTROYED_LEFT_RIGHT = L'│',
		CONSTRUCTION_DESTROYED_UP_DOWN = L'─',
		CONSTRUCTION_DESTROYED_UP_LEFT = L'┌',
		CONSTRUCTION_DESTROYED_UP_RIGHT = L'┐',
		CONSTRUCTION_DESTROYED_DOWN_LEFT = L'└',
		CONSTRUCTION_DESTROYED_DOWN_RIGHT = L'┘',

		BULLET = L'•',
		BULLET_UP = L'↥',
		BULLET_DOWN = L'↧',
		BULLET_LEFT = L'↤',
		BULLET_RIGHT = L'↦',

		AI_TANK_UP = L'?',
		AI_TANK_RIGHT = L'»',
		AI_TANK_DOWN = L'¿',
		AI_TANK_LEFT = L'«',

		OTHER_TANK_UP = L'˄',
		OTHER_TANK_RIGHT = L'˃',
		OTHER_TANK_DOWN = L'˅',
		OTHER_TANK_LEFT = L'˂',

		TANK_UP = L'▲',
		TANK_RIGHT = L'►',
		TANK_DOWN = L'▼',
		TANK_LEFT = L'◄',

		WORM_HOLE = L'ʘ',

		BOG = L'@',
		SAND = L'□',
		MOAT_HORIZONTAL = L'=',
		MOAT_VERTICAL = L'‖',
		HEDGEHOG = L'ͱ',
		BATTLE_WALL = L'☼',
		BANG = L'Ѡ',

		NONE = L' ',

		BONUS_AMMO = '◊',
		MEDKIT = '☺'
	};
	tile_id_ id_;
	enum tile_type_ {
		type_enemy,
		type_player,
		type_me,
		type_breakable,
		type_pickable,
		type_pit,
		type_unbreakable,
		type_none,
		type_bullet,
		type_tank,
	};
public:
	tile() : id_(NONE) {}
	tile(tile_id_ id) : id_(id) {}
	tile_id_ get_id() const { return id_; }
	//bool is_ladder() {	return id_ == LADDER || id_ == ENEMY_LADDER || id_ == HERO_LADDER || id_ == OTHER_HERO_LADDER || id_ == OTHER_HERO_SHADOW_LADDER || id_ == HERO_SHADOW_LADDER; }
	bool is_me() const {	return id_ == TANK_UP || id_ == TANK_RIGHT || id_ == TANK_DOWN || id_ == TANK_LEFT; }
	bool is_bullet()const { return id_ == BULLET || id_ == BULLET_DOWN || id_ == BULLET_LEFT || id_ == BULLET_RIGHT || id_ == BULLET_UP; }
	bool is_pit() const { return id_ == BOG; }
	bool is_enemy_bot() const { return id_ == AI_TANK_UP || id_ == AI_TANK_RIGHT || id_ == AI_TANK_DOWN || id_ == AI_TANK_LEFT; }
	bool is_player() const { return  id_ == OTHER_TANK_UP || id_ == OTHER_TANK_RIGHT || id_ == OTHER_TANK_DOWN || id_ == OTHER_TANK_LEFT; }
	bool is_unbreakable() const { return id_ == BATTLE_WALL; }
	bool is_tank() const { return get_type() == type_enemy || get_type() == type_player; }
	player_command get_tank_direction() const {
		switch (id_) {
		case AI_TANK_DOWN:
		case OTHER_TANK_DOWN:
		case TANK_DOWN:
			return player_command::down;
		case AI_TANK_LEFT:
		case OTHER_TANK_LEFT:
		case TANK_LEFT:
			return player_command::left;
		case AI_TANK_RIGHT:
		case OTHER_TANK_RIGHT:
		case TANK_RIGHT:
			return player_command::right;
		case AI_TANK_UP:
		case OTHER_TANK_UP:
		case TANK_UP:
			return player_command::up;
		default:
			return player_command::idle;
		}
	}
	int get_cost(player_command cmd) const {
		if (!is_brick()) return 1;
		switch (id_) {
		case CONSTRUCTION:
			return 13;
		case CONSTRUCTION_DESTROYED_DOWN:
		case CONSTRUCTION_DESTROYED_UP:
			if (cmd == player_command::down || cmd == player_command::up)
				return 2;
			return 13;
		case CONSTRUCTION_DESTROYED_UP_DOWN:
		case CONSTRUCTION_DESTROYED_DOWN_TWICE:
		case CONSTRUCTION_DESTROYED_UP_TWICE:
			if (cmd == player_command::down || cmd == player_command::up)
				return 2;
			return 13;
		case CONSTRUCTION_DESTROYED_DOWN_RIGHT:
		case CONSTRUCTION_DESTROYED_DOWN_LEFT:
		case CONSTRUCTION_DESTROYED_UP_LEFT:
		case CONSTRUCTION_DESTROYED_UP_RIGHT:
				return 2;
		case CONSTRUCTION_DESTROYED_LEFT:
		case CONSTRUCTION_DESTROYED_RIGHT_TWICE:
		case CONSTRUCTION_DESTROYED_LEFT_TWICE:
		case CONSTRUCTION_DESTROYED_RIGHT:
			if (cmd == player_command::left || cmd == player_command::right)
				return 2;
			return 13;
		}
	}
	//bool can_pass_trough() { return id_ == HERO_FALL_LEFT || id_ == HERO_FALL_RIGHT || id_ == HERO_LEFT || id_ == HERO_RIGHT || id_ == tile::NONE || is_gold() || id_ == tile::PIPE; }
	bool is_brick() const { return id_ == CONSTRUCTION || id_ == CONSTRUCTION_DESTROYED_DOWN || id_ == CONSTRUCTION_DESTROYED_DOWN_LEFT ||
		id_ == CONSTRUCTION_DESTROYED_DOWN_RIGHT || id_ == CONSTRUCTION_DESTROYED_DOWN_TWICE || id_ == CONSTRUCTION_DESTROYED_LEFT ||
		id_ == CONSTRUCTION_DESTROYED_LEFT_RIGHT || id_ == CONSTRUCTION_DESTROYED_LEFT_TWICE || id_ == CONSTRUCTION_DESTROYED_RIGHT ||
		id_ == CONSTRUCTION_DESTROYED_RIGHT_TWICE || id_ == CONSTRUCTION_DESTROYED_UP || id_ == CONSTRUCTION_DESTROYED_UP_DOWN ||
		id_ == CONSTRUCTION_DESTROYED_UP_LEFT || id_ == CONSTRUCTION_DESTROYED_UP_RIGHT || id_ == CONSTRUCTION_DESTROYED_UP_TWICE; }
	tile_type_ get_type() const {
		if (is_enemy_bot()) return type_enemy;
		if (is_player()) return type_player;
		if (is_me()) return type_me;
		if (is_unbreakable()) return type_unbreakable;
		if (is_brick()) return type_breakable;
		if (is_pit()) return type_pit;
		if (id_ == NONE) return type_none;
		if (is_bullet()) return type_bullet;
	}
	tile& operator=(const tile& l) { this->id_ = l.id_; return *this; }
	operator tile_id_() { return get_id(); }
};

static bool operator==(const tile& lhs, const tile& rhs) {
	return lhs.get_id() == rhs.get_id();
}

static bool operator==(const tile& lhs, const tile::tile_id_& rhs) {
	return lhs.get_id() == rhs;
}