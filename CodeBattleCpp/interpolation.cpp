#include "interpolation.hpp"
#include <iostream>
#include <queue>
#include <map>

#define MAX_CHOKE 3

static constexpr std::array xoffsets = { 1,-1,0,0 };
static constexpr std::array yoffsets = { 0,0,-1,1 };
int choked = 0;

interpolation::interpolation() {

}

interpolation::~interpolation() {

}

void interpolation::predict_bullets(vec pos, vec velocity) {
	for (int k = 1; k <= 2; k++) {
		if (pos.y + velocity.y * k >= pred_field.size() || pos.x + velocity.x * k >= pred_field[pos.y].size() || pos.y + velocity.y * k < 0 || pos.x + velocity.x * k < 0)
			break;
		auto& element = pred_field[pos.y + velocity.y * k][pos.x + velocity.x * k];
		if (element.is_brick() || element.is_unbreakable())
			break;
		if (element.is_me()) {
			if (pos.y + velocity.y * (k - 1) >= 0 || pos.y + velocity.y * (k - 1) < pred_field.size() || pos.x + velocity.x * (k - 1) < pred_field[pos.y].size() || pos.x + velocity.x * (k - 1) >= 0) {
				pred_field[pos.y + velocity.y * (k - 1)][pos.x + velocity.x * (k - 1)] = pred_field[pos.y][pos.x];
				bullet_velocities[{pos.x + velocity.x * (k - 1), pos.y + velocity.y * (k - 1)}].push_back(velocity);
			}
			if (pos.y + velocity.y * (k + 1) >= 0 || pos.y + velocity.y * (k + 1) < pred_field.size() || pos.x + velocity.x * (k + 1) < pred_field[pos.y].size() || pos.x + velocity.x * (k + 1) >= 0) {
				pred_field[pos.y + velocity.y * (k + 1)][pos.x + velocity.x * (k + 1)] = pred_field[pos.y][pos.x];
				bullet_velocities[{pos.x + velocity.x * (k + 1), pos.y + velocity.y * (k + 1)}].push_back(velocity);
			}
			bullet_velocities[{pos.x, pos.y}].push_back(velocity);
		}
		else {
			pred_field[pos.y + velocity.y * k][pos.x + velocity.x * k] = pred_field[pos.y][pos.x];
			bullet_velocities[{pos.x + velocity.x * k, pos.y + velocity.y * k}].push_back(velocity);
		}
	}
}

void interpolation::predict_field() {
	if (old_fields.size() < 2)
	{
		predicted_ = std::make_shared<game_board>(old_fields.back()->get_board());
		predicted_->bfs(predicted_->get_myself());
		old_cmd.push_front(predicted_->current_act());
		return;
	}
	else {
		auto first = old_fields.back();
		old_fields.pop_back();
		auto second = old_fields.back();
		old_fields.push_back(first);
		if (second->get_board().size() != first->get_board().size()) {
			old_fields.clear();
			old_fields.push_back(first);
			predicted_ = std::make_shared<game_board>(old_fields.back()->get_board());
			predicted_->bfs(predicted_->get_myself());
			old_cmd.push_front(predicted_->current_act());
			return;
		}
	}
	static std::array<tile, 4> directions_ai = {
		tile::AI_TANK_LEFT,
		tile::AI_TANK_RIGHT,
		tile::AI_TANK_DOWN,
		tile::AI_TANK_UP,
	};
	static std::array<tile,4> directions_players = {
		tile::OTHER_TANK_LEFT,
		tile::OTHER_TANK_RIGHT,
		tile::OTHER_TANK_DOWN,
		tile::OTHER_TANK_UP,
	};
	auto first = old_fields.back();
	old_fields.pop_back();
	auto second = old_fields.back();
	old_fields.push_back(first);
	auto first_field = first->get_board();
	auto second_field = second->get_board();
	pred_field = first_field;
	std::cout << old_cmd.size() << std::endl;
	static std::map<player_command, vec> cmd_directions = {
			{player_command::down,vec(0,1)},
			{player_command::up,vec(0,-1)},
			{player_command::left,vec(-1,0)},
			{player_command::right,vec(1,0)},
	};
	if (false && old_cmd.size())
	{

		vec velocity(0, 0);
		if (cmd_directions.count(old_cmd.back()))
			velocity = cmd_directions[old_cmd.back()];
		auto me = first->get_myself();
		if (me.x == 0 || old_cmd.size() == MAX_CHOKE)
			old_cmd.clear();
		if (old_cmd.size()) {
			for (auto& elem : old_cmd) {
				velocity = cmd_directions[elem];
				if (pred_field[me.y + velocity.y][me.x + velocity.x].is_brick() ||
					pred_field[me.y + velocity.y][me.x + velocity.x].is_unbreakable() ||
					pred_field[me.y + velocity.y][me.x + velocity.x].is_player() ||
					pred_field[me.y + velocity.y][me.x + velocity.x].is_enemy_bot())
					continue;
				auto old = pred_field[me.y][me.x];
				pred_field[me.y][me.x] = tile::NONE;
				switch (elem) {
				case player_command::down:
					pred_field[me.y + velocity.y][me.x + velocity.x] = tile::TANK_DOWN;
					break;
				case player_command::up:
					pred_field[me.y + velocity.y][me.x + velocity.x] = tile::TANK_UP;
					break;
				case player_command::left:
					pred_field[me.y + velocity.y][me.x + velocity.x] = tile::TANK_LEFT;
					break;
				case player_command::right:
					pred_field[me.y + velocity.y][me.x + velocity.x] = tile::TANK_RIGHT;
					break;
				default:
					pred_field[me.y + velocity.y][me.x + velocity.x] = old;
				}
				me += velocity;
			}
			old_cmd.pop_back();
		}
	}
	for (auto i = 0; i < first_field.size(); i++) {
		for (auto j = 0; j < first_field[i].size(); j++) {
			if (first_field[i][j].is_bullet()) {
				vec velocity(0, 0);

				for (auto k = 0; k < xoffsets.size(); k++) {
					if (i + yoffsets[k] * 2 >= second_field.size() || j + xoffsets[k] * 2 >= second_field.size() || j + xoffsets[k] * 2 < 0 || i + yoffsets[k] * 2 < 0)
						continue;
					if (second_field[i + yoffsets[k] * 2][j + xoffsets[k] * 2].is_bullet() ||
						second_field[i + yoffsets[k] * 2][j + xoffsets[k] * 2].is_bullet()) {
						velocity.x = -xoffsets[k];
						velocity.y = -yoffsets[k];
						break;
					}
				}
				if (i + velocity.y * 2 >= second_field.size() || j + velocity.x * 2 >= second_field.size() || j + velocity.x * 2 < 0 || i + velocity.y * 2 < 0)
					continue;
				if (velocity.x != 0 || velocity.y != 0)
					predict_bullets(vec(j, i), velocity);
				else {
					for (auto& p : cmd_directions) {
						for (auto k = 1; k <= 2; k++) {
							if (i + p.second.y * k < 0 || i + p.second.y * k >= first_field.size() || j + p.second.x * k < 0 && j + p.second.x * k >= first_field.size())
								break;
							if (first_field[i + p.second.y * k][j + p.second.x * k].is_tank())
								if (static_cast<player_command>(static_cast<int>(first_field[i + p.second.y * k][j + p.second.x * k].get_tank_direction()) ^ 1) == p.first)
									predict_bullets(vec(j, i), cmd_directions[static_cast<player_command>(static_cast<int>(first_field[i + p.second.y * k][j + p.second.x * k].get_tank_direction())/* ^ 1*/)]);

						}
					}
				}
			}
		}
	}
	for (auto i = 0; i < first_field.size(); i++) {
		for (auto j = 0; j < first_field[i].size(); j++) {
			if (first_field[i][j].is_enemy_bot() || first_field[i][j].is_player()) {
				vec velocity(0, 0);

				for (auto k = 0; k < 4; k++) {
					if ((second_field[i + yoffsets[k]][j + xoffsets[k]].is_enemy_bot() && directions_ai[k] == first_field[i][j]) || (second_field[i + yoffsets[k]][j + xoffsets[k]].is_player() && directions_players[k] == first_field[i][j])) {
						velocity.x = -xoffsets[k];
						velocity.y = -yoffsets[k];
						break;
					}
				}
				auto it = tank_vels.find({ j,i });
				if (it == tank_vels.end() && velocity.x == 0 && velocity.y == 0)
					continue;
				else if(it != tank_vels.end())
					velocity = it->second;
				if (velocity.x != 0 || velocity.y != 0) {
					for (auto g = 1; g <= 1; g++) {
						velocity.x *= g;
						velocity.y *= g;
						if (i + velocity.y >= pred_field.size() || j + velocity.x >= pred_field[0].size() || j + velocity.x < 0 || i + velocity.y < 0)
							break;
						if (pred_field[i + velocity.y][j + velocity.x] == tile::NONE || pred_field[i + velocity.y][j + velocity.x] == tile::BULLET) {
							pred_field[i + velocity.y][j + velocity.x] = first_field[i][j];
							continue;
						}
					}
				}

			}
		}
	}


	predicted_ = std::make_shared<game_board>(pred_field);
	predicted_->bfs(predicted_->get_myself());
}