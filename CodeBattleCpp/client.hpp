#pragma once

#include <string>
#include <thread>
#include "easywsclient\easywsclient.hpp"
#ifdef _WIN32
#pragma comment( lib, "ws2_32" )
#include <WinSock2.h>
#endif
#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <string_view>
#include <functional>
#include <memory>
#include <mutex>

#include "interpolation.hpp"
#include "tile.hpp"
#include "player_command.hpp"
#include "game_board.hpp"


class tile;

class client
{
	/*tile **map;*/
	std::vector<std::vector<tile>> map_;
	std::shared_ptr<game_board> board;
	//uint32_t /*map_size, */player_x, player_y;

	easywsclient::WebSocket *web_socket;
	std::string path, statistic_info, user_id;

	bool is_running;
	std::thread *work_thread;
	void update_func(std::function<void()> _message_handler);

public:
 	client(std::string_view _server, std::string_view _userEmail, std::string_view _userPassword = "");
	~client();

	void run(std::function<void()> _message_handler);
	void send(int action) {
		auto move_act = action & 0xFFFF;
		std::string ans = "";
		switch (static_cast<player_command>(move_act))
		{
		//case player_command::dig_left:
		//	return;
		case player_command::dig_right:
			ans = (std::string("RIGHT"));
			break;
		case player_command::down:
			ans = (std::string("DOWN"));
			break;
		case player_command::up:
			ans = (std::string("UP"));
			break;
		case player_command::left:
			ans = (std::string("LEFT"));
			break;
		case player_command::right:
			ans = (std::string("RIGHT"));
			break;
		case player_command::kill_self:
			ans = (std::string(""));
			break;
		case player_command::dig:
			ans = (std::string("ACT"));
			break;
		default:
			ans = (std::string(""));
			break;
		}
		auto shoot_act = static_cast<shoot_command>(action >> 16);
		if (shoot_act == shoot_command::after)
			ans += ",ACT";
		if (shoot_act == shoot_command::before)
			ans = "ACT," + ans;
		send(ans);
	}
	std::shared_ptr<game_board> get_game_board() { return board; }
	const std::vector<std::vector<tile>>& get_map() { return map_; }
	void lock() { board_lock.lock(); }
	void unlock() { board_lock.unlock(); }
	bool try_lock() { return board_lock.try_lock(); }
private:
	std::mutex board_lock;
	void send(std::string msg)
	{
		std::cout << "Sending: " << msg << std::endl;
		web_socket->send(msg);
	}
};
