#include "client.hpp"
#include "graphics.hpp"


using namespace std::chrono_literals;

static constexpr std::array<std::pair<std::wstring_view, tile::tile_id_>, 45> texture_paths_ = {
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/ai_tank_down.png",tile::AI_TANK_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/ai_tank_left.png",tile::AI_TANK_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/ai_tank_right.png",tile::AI_TANK_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/ai_tank_up.png",tile::AI_TANK_UP},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bang.png",tile::BANG},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/battle_wall.png",tile::BATTLE_WALL},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bullet.png",tile::BULLET_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bullet.png",tile::BULLET_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bullet.png",tile::BULLET_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bullet.png",tile::BULLET_UP},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/bullet.png",tile::BULLET},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction.png",tile::CONSTRUCTION},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_down.png",tile::CONSTRUCTION_DESTROYED_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_down_left.png",tile::CONSTRUCTION_DESTROYED_DOWN_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_down_right.png",tile::CONSTRUCTION_DESTROYED_DOWN_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_down_twice.png",tile::CONSTRUCTION_DESTROYED_DOWN_TWICE},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_left.png",tile::CONSTRUCTION_DESTROYED_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_left_right.png",tile::CONSTRUCTION_DESTROYED_LEFT_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_left_twice.png",tile::CONSTRUCTION_DESTROYED_LEFT_TWICE},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_right.png",tile::CONSTRUCTION_DESTROYED_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_right_twice.png",tile::CONSTRUCTION_DESTROYED_RIGHT_TWICE},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_right_up.png",tile::CONSTRUCTION_DESTROYED_UP_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_up.png",tile::CONSTRUCTION_DESTROYED_UP},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_up_down.png",tile::CONSTRUCTION_DESTROYED_UP_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_up_left.png",tile::CONSTRUCTION_DESTROYED_UP_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/construction_destroyed_up_twice.png",tile::CONSTRUCTION_DESTROYED_UP_TWICE},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/none.png",tile::NONE},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/other_tank_down.png",tile::OTHER_TANK_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/other_tank_left.png",tile::OTHER_TANK_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/other_tank_right.png",tile::OTHER_TANK_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/other_tank_up.png",tile::OTHER_TANK_UP},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/tank_down.png",tile::TANK_DOWN},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/tank_left.png",tile::TANK_LEFT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/tank_right.png",tile::TANK_RIGHT},
	std::pair<std::wstring_view, tile::tile_id_>{L"resources/tank_up.png",tile::TANK_UP},
};


void graphics::invalidate() {
	D3DXCreateSprite(device_, &sprite_);
	for (auto& elem : texture_paths_)
		D3DXCreateTextureFromFile(device_, elem.first.data(), &textures_[elem.second]);

}

graphics::graphics(HWND hwnd) {
	if ((pd3d = Direct3DCreate9(D3D_SDK_VERSION)) == nullptr) {
		std::cout << "pD3D == nullptr\n";
		return;
	}
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferCount = D3DFMT_UNKNOWN;
	d3dpp.EnableAutoDepthStencil = true;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;


	if (pd3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &device_) < 0) {
		pd3d->Release();
		pd3d = nullptr;
		std::cout << "device is dead :c";
		return;
	}
	invalidate();
}

void graphics::render_scene(std::shared_ptr<client>& gcb) {
	device_->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(255, 0, 0, 0), 1.f, 0);
	auto hresult = device_->TestCooperativeLevel();
	if (hresult == D3DERR_DEVICELOST || hresult == D3DERR_DEVICENOTRESET) {
		device_->Reset(&d3dpp);
		sprite_->Release();
		sprite_ = nullptr;
		for (auto& tex : textures_)
		{
			if(tex.second)
				tex.second->Release();
			tex.second = nullptr;
		}

		if (!FAILED(device_->Reset(&d3dpp)))
			invalidate();

		else return;
	}
	auto result = device_->BeginScene();
	if (SUCCEEDED(result)) {
		std::vector t(gcb->get_map());
		struct {
			float x, y;
		} pos{ 0,0 };
		std::vector<vec> t_path;
		if (gcb->try_lock()) {
			if (gcb->get_game_board().get())
				t_path = gcb->get_game_board()->get_current_path();
			gcb->unlock();
		}
		for (auto& line : t) {
			for (auto& row : line) {
				auto texture = textures_[row.get_id()];
				if (!texture)
					texture = textures_[tile::NONE];
				D3DXMATRIX world;
				D3DXMATRIX scale;
				D3DXMATRIX translation;
				D3DXMATRIX rotation;
				D3DXMatrixIdentity(&world);
				D3DSURFACE_DESC img_info;
				texture->GetLevelDesc(0, &img_info);
				float scl_x = /*16.f / float(img_info.Width)*/.4f;
				float scl_y = /*16.f / float(img_info.Height)*/.4f;
				D3DXMatrixScaling(&scale, scl_x, scl_y, 1.f);
				D3DXMatrixTranslation(&translation, 0.f, 0.f, 0.f);
				D3DXMatrixRotationYawPitchRoll(&rotation, 0.f, 0.f, 0.f);
				world = scale * translation;
				auto vec = D3DXVECTOR3(pos.x, pos.y, 0.f);
				sprite_->SetTransform(&world);
				sprite_->Begin(D3DXSPRITE_ALPHABLEND);
				auto color = D3DCOLOR_RGBA(255, 255, 255, 255);
				sprite_->Draw(texture, nullptr, nullptr, &vec, color);
				sprite_->End();
				pos.x += 32;
			}
			pos.x = 0;
			pos.y += 32;
		}
		if (t_path.size()) {
			auto prev = gcb->get_game_board()->get_myself();
			prev.y = prev.y * 32.f*0.4f + 32.f * 0.4f / 2.f;
			prev.x = prev.x * 32.f * 0.4f + 32.f * 0.4f / 2.f;
			for (auto point : t_path) {
				point.y = point.y * 32.f * 0.4f + 32.f * 0.4f / 2.f;
				point.x = point.x * 32.f * 0.4f + 32.f * 0.4f / 2.f;
				draw_line(vec(prev.x - 1, prev.y - 1), vec(point.x - 1, point.y - 1), D3DCOLOR_ARGB(255, 0, 240, 0));
				draw_line(prev, point, D3DCOLOR_ARGB(255, 0, 240, 0));
				draw_line(vec(prev.x + 1, prev.y + 1), vec(point.x + 1, point.y + 1), D3DCOLOR_ARGB(255, 0, 240, 0));
				prev = point;
			}
		}
		device_->EndScene();
	}
	result = device_->Present(NULL, NULL, NULL, NULL);
	if (!SUCCEEDED(result)) MessageBox(0, 0, 0, 0);
	std::this_thread::sleep_for(1ms);
}

void graphics::draw_line(vec p1, vec p2, uint32_t col) {

	vertex vert[2] =
	{
		{ p1.x, p1.y, 0.0f, 1.0f, col },
		{ p2.x, p2.y, 0.0f, 1.0f, col }
	};

	device_->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);
	device_->DrawPrimitiveUP(D3DPT_LINELIST, 1, &vert, sizeof(vertex));

}

graphics::~graphics() {
	device_->Release();
	sprite_->Release();
	sprite_ = nullptr;
	for (auto& tex : textures_)
	{
		if (tex.second)
			tex.second->Release();
		tex.second = nullptr;
	}
}