#pragma once
#include <cmath>

class vec
{
public:
	vec() : x(0), y(0) {}
	vec(int x, int y);
	~vec();

	vec shiftLeft(int delta);
	vec shiftLeft();
	vec shiftRight(int delta);
	vec shiftRight();
	vec shiftBottom(int delta);
	vec shiftBottom();
	vec shiftTop(int delta);
	vec shiftTop();
	bool isOutOfBoard(int size);
	void print();
	float dist_to(const vec& to) const {	return std::sqrtf((x - to.x)*(x - to.x) + (y - to.y)*(y - to.y)); }
	int x;
	int y;

	vec& operator+=(const vec& rhs) {
		this->x += rhs.x;
		this->y += rhs.y;
		return *this;
	}
	vec& operator-=(const vec& rhs) {
		this->x -= rhs.x;
		this->y -= rhs.y;
		return *this;
	}
};

static vec operator+(const vec& lhs, const vec& rhs) {
	auto buf = lhs;
	buf += rhs;
	return buf;
}

static bool operator<(const vec& lhs, const int& rhs) {
	return lhs.x < rhs && lhs.y < rhs;
}

static bool operator>(const vec& lhs, const int& rhs) {
	return lhs.x > rhs && lhs.y > rhs;
}

static bool operator==(const vec& lhs, const vec& rhs) {
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

static bool operator!=(const vec& lhs, const vec& rhs) {
	return lhs.x != rhs.x || lhs.y != rhs.y;
}